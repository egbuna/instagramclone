package com.example.raynold.instagramclone.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.raynold.instagramclone.fragments.CameraFragment;
import com.example.raynold.instagramclone.fragments.EmailFragment;
import com.example.raynold.instagramclone.fragments.NotificationFragment;
import com.example.raynold.instagramclone.fragments.PhoneFragment;
import com.example.raynold.instagramclone.fragments.ProfileFragment;
import com.example.raynold.instagramclone.fragments.SearchFragment;
import com.example.raynold.instagramclone.fragments.TimelineFragment;

public class BottomTabAdapter extends FragmentPagerAdapter {

    Context context;


    public BottomTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0 )
            return TimelineFragment.getInstance();
        if (position == 1)
            return SearchFragment.getInstance();
        if (position == 2)
            return CameraFragment.getInstance();
        if (position == 3)
            return NotificationFragment.getInstance();
        else if (position == 4)
            return ProfileFragment.getInstance();
        return ProfileFragment.getInstance();
    }

    @Override
    public int getCount() {
        return 5;
    }

//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//
//        switch (position){
//
//            case 0:
//                return "Home";
//            case 1:
//                return "Search";
//            case 2:
//                return "Camera";
//            case 3:
//                return "Likes";
//            case 4:
//                return "Profile";
//        }
//        return super.getPageTitle(position);
//    }
}
