package com.example.raynold.instagramclone.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.raynold.instagramclone.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneFragment extends Fragment {

    public static PhoneFragment instance = null;


    public PhoneFragment() {
        // Required empty public constructor
    }

    public static PhoneFragment getInstance(){

        if (instance == null)
            instance = new PhoneFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_phone, container, false);
    }

}
