package com.example.raynold.instagramclone.utils;

import android.content.Context;

import com.example.raynold.instagramclone.R;
import com.example.raynold.instagramclone.model.UsersPost;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static int[] getIcons = {
            R.drawable.ic_home_black_24dp,
            R.drawable.ic_search_black_24dp,
            R.drawable.ic_photo_camera_black_24dp,
            R.drawable.ic_location_on_black_24dp,
            R.drawable.ic_person_outline_black_24dp
    };

    public static List<UsersPost> getUsersPost(Context context){

        List<UsersPost> usersPostList = new ArrayList<>();
        usersPostList.add(new UsersPost("Raynold", R.drawable.pic, R.drawable.pic, 12,context.getResources().getString(R.string.placeholder_text) ));
        usersPostList.add(new UsersPost("Sandra", R.drawable.pic, R.drawable.pic, 2000, context.getResources().getString(R.string.placeholder_text)));
        usersPostList.add(new UsersPost("Chuma", R.drawable.pic, R.drawable.pic ,20, context.getResources().getString(R.string.placeholder_text)));
        usersPostList.add(new UsersPost("Ivy", R.drawable.pic, R.drawable.pic, 5000, context.getResources().getString(R.string.placeholder_text)));
        usersPostList.add(new UsersPost("Chukwudi", R.drawable.pic, R.drawable.pic, 10,context.getResources().getString(R.string.placeholder_text)));

        return usersPostList;
    }

}
