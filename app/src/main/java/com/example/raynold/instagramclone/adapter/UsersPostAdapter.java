package com.example.raynold.instagramclone.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.raynold.instagramclone.R;
import com.example.raynold.instagramclone.model.UsersPost;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UsersPostAdapter extends RecyclerView.Adapter<UsersPostAdapter.ViewHolder>{

    Context mContext;
    List<UsersPost> mUsersPostList;

    public UsersPostAdapter(Context context, List<UsersPost> usersPostList) {
        mContext = context;
        mUsersPostList = usersPostList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.timeline_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        UsersPost usersPost = mUsersPostList.get(position);
        holder.bind(usersPost);
    }

    @Override
    public int getItemCount() {
        return mUsersPostList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.timeline_user_pic)
        CircleImageView mTimelinePic;
        @BindView(R.id.timeline_user_likes_number)
        TextView mTextLikes;
        @BindView(R.id.timeline_user_name)
        TextView mTextUsername;
        @BindView(R.id.timeline_user_like_btn)
        ImageButton mLikeBtn;
        @BindView(R.id.timeline_user_post)
        ImageView mUsersPostPic;
        @BindView(R.id.timeline_user_post_text)
        TextView mUserPostText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(UsersPost usersPost) {

            mTextUsername.setText(usersPost.getUsername());
            mTextLikes.setText(new StringBuilder().append(usersPost.getLikes()).append(" likes"));
            mUserPostText.setText(new StringBuilder().append(usersPost.getUsername())
                    .append(usersPost.getUserPostText()));

            Picasso.with(mContext).load(usersPost.getUserPic()).into(mTimelinePic);
            Picasso.with(mContext).load(usersPost.getUserPost()).into(mUsersPostPic);


        }
    }
}
