package com.example.raynold.instagramclone.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.example.raynold.instagramclone.R;
import com.example.raynold.instagramclone.adapter.BottomTabAdapter;
import com.example.raynold.instagramclone.adapter.UsersPostAdapter;
import com.example.raynold.instagramclone.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    UsersPostAdapter mUsersPostAdapter;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);


        mAuth = FirebaseAuth.getInstance();

        BottomTabAdapter bottomTabAdapter = new BottomTabAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(bottomTabAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        setUpTabIcon();


        if (mAuth.getCurrentUser() == null);
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public void setUpTabIcon(){
        mTabLayout.getTabAt(0).setIcon(Utils.getIcons[0]);
        mTabLayout.getTabAt(1).setIcon(Utils.getIcons[1]);
        mTabLayout.getTabAt(2).setIcon(Utils.getIcons[2]);
        mTabLayout.getTabAt(3).setIcon(Utils.getIcons[3]);
        mTabLayout.getTabAt(4).setIcon(Utils.getIcons[4]);
    }
}
