package com.example.raynold.instagramclone.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.raynold.instagramclone.fragments.EmailFragment;
import com.example.raynold.instagramclone.fragments.PhoneFragment;

public class FragmentAdapter extends FragmentPagerAdapter {

    Context mContext;

    public FragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0 )
            return PhoneFragment.getInstance();
        else if (position == 1)
            return EmailFragment.getInstance();
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Phone";
            case 1:
                return "Email";
        }
        return "";
    }
}
