package com.example.raynold.instagramclone.model;

public class UsersPost {

    private String username;
    private int userPic;
    private int userPostPic;
    private int likes;
    private String userPostText;

    public UsersPost() {
    }

    public UsersPost(String username, int userPic, int userPostPic, int likes, String userPostText) {
        this.username = username;
        this.userPic = userPic;
        this.userPostPic = userPostPic;
        this.likes = likes;
        this.userPostText = userPostText;
    }

    public int getUserPostPic() {
        return userPostPic;
    }

    public void setUserPostPic(int userPostPic) {
        this.userPostPic = userPostPic;
    }

    public String getUserPostText() {
        return userPostText;
    }

    public void setUserPostText(String userPostText) {
        this.userPostText = userPostText;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserPic() {
        return userPic;
    }

    public void setUserPic(int userPic) {
        this.userPic = userPic;
    }

    public int getUserPost() {
        return userPostPic;
    }

    public void setUserPost(int userPost) {
        this.userPostPic = userPost;
    }
}
