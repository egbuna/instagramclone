package com.example.raynold.instagramclone.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ProfileFragmentAdapter extends FragmentPagerAdapter {


    public ProfileFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }
}
