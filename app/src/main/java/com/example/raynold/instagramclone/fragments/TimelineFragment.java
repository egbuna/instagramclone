package com.example.raynold.instagramclone.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.raynold.instagramclone.R;
import com.example.raynold.instagramclone.adapter.UsersPostAdapter;
import com.example.raynold.instagramclone.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment {

    public static TimelineFragment instance = null;
    @BindView(R.id.users_posts)
    RecyclerView mUserPostRecycler;


    public TimelineFragment() {
        // Required empty public constructor
    }

    public static TimelineFragment getInstance(){

        if (instance == null)
            instance = new TimelineFragment();

        return instance;


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, view);

        mUserPostRecycler.setHasFixedSize(true);
        mUserPostRecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mUserPostRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
        mUserPostRecycler.setAdapter(new UsersPostAdapter(getContext() , Utils.getUsersPost(getContext())));

        // Inflate the layout for this fragment
        return view;
    }

}
