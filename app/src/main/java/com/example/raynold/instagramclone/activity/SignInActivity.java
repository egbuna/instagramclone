package com.example.raynold.instagramclone.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.raynold.instagramclone.R;

public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
    }

    public void gotoRegister(View view) {
        startActivity(new Intent(SignInActivity.this, RegisterActivity.class));
    }

    public void gotoLogIn(View view) {
        startActivity(new Intent(SignInActivity.this, LoginActivity.class));
    }
}
