package com.example.raynold.instagramclone.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.raynold.instagramclone.R;
import com.example.raynold.instagramclone.activity.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmailFragment extends Fragment {

    public static EmailFragment instance = null;

    @BindView(R.id.register_email)
    EditText mRegEmail;
    @BindView(R.id.register_password)
    EditText mRegPassword;
    @BindView(R.id.register_retype_password)
    EditText mRegConfirmPassword;
    @BindView(R.id.register_next_btn)
    Button mRegBtn;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    FirebaseAuth mAuth;


    public EmailFragment() {
        // Required empty public constructor
    }


    public static EmailFragment getInstance(){

        if (instance == null)
            instance = new EmailFragment();
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_email, container, false);
        ButterKnife.bind(this, view);

        mAuth = FirebaseAuth.getInstance();

        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateUserInput();
            }
        });

        return view;
    }

    private void validateUserInput() {

        String email = mRegEmail.getText().toString();
        String password = mRegPassword.getText().toString();
        String retypedPassword = mRegConfirmPassword.getText().toString();

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(retypedPassword)){

            if (password != retypedPassword){

                registerUser(email, password);
            } else {
                Toast.makeText(getContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Fill in forms correctly", Toast.LENGTH_SHORT).show();
        }
    }

    private void registerUser(String email, String password) {


        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    startActivity(new Intent(getContext(), MainActivity.class));
                    getActivity().finish();
                } else if (!task.isSuccessful()){
                    Toast.makeText(getActivity(), "Failed to register user", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
